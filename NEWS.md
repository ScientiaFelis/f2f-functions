
# *NEWS*

# F2Ffunctions 0.7.0 (2022-11-09)

**save_F2F_plot()**

- Change pixels to inches for `width` and `height`

**fonts**

- Remove the Garamond and Frutiger fonts

**theme_F2F**

- Change `size` to `linewidth` for line elements following ggplot2() 3.4.0 update


# F2Ffunctions 0.5.2 (2020-10-25)

**theme_F2F**

- Added a bottom alignment for the *strip.text*
- Adjusted the margins around the strip text

# F2Ffunctions 0.5.1 (2020-07-30)

**theme_F2F**

- Set the *strip.text.x/y* *bottom, top, left, right* to NULL

**scale_fill_F2F and scale_colour_F2F**

- Removed the *secondary* and *both* palettes
- Added a colour blind palette *colblind*


# F2Ffunctions 0.3.3

**theme_F2F**

- Stated a hjust=1 in the *axis.text.y*
- Added an alpha of 0.4 to the *x* and *y* *panel.grid.major* colours
- Changed the *plot.title* position to *plot*


# F2Ffunctions 0.3.2

- First packaged version