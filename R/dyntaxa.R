#' Names in Latin and Swedish of Plant Species, Genus and Families.
#'
#' A data table containing the names of different \strong{Plant} taxa in both Latin and Swedish. It is used for translating the Swedish names into Latin.
#' dyntaxa.
#' @note Downloaded the 2020-01-14
#' @usage dyntaxa
#' @format A data frame with 11128 rows and 5 variables:
#' \describe{
#'   \item{Vetenskapligt namn}{Latin name on  taxa.}
#'   \item{Auktor}{Who named /described the taxa.}
#'   \item{Svenskt namn}{The Swedish name of the taxa.}
#'   \item{TaxonId}{ID number of Taxon}
#'   \item{URL till taxoninformation}{The ther webb adress to the taxon information in dyntaxa..}
#' }
#' @source \url{https://www.dyntaxa.se/}
"dyntaxa"