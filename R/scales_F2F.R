
  ## Assign a different palette to the plots
  ##
  #my_palette <- c("#21a117",  "#ED0000B2","#00468BB2","#876880", "#925E8FB2", "#149ddf","#58a834", "#ff7700", "#e6ecf0")
#  my_palette <- c("#B3E2CD", "#FDCDAC", "#CBD5E8", "#F4CAE4", "#E6F5C9")
  ## The assign function puts my_palette in the global environment for ggplot to use as colours in the figures.

 # assign("scale_colour_discrete", function(..., values = my_palette) scale_colour_manual(..., values = values), globalenv()) #
 #  assign("scale_fill_discrete", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv()) #


F2F_colours <- c(
  "turquise" = "#88DAE1", #"#B3E2CD",
  "orange" = "#FDCDAC",
  "blue" = "#CBD5E8",
  "rosa" = "#F4CAE4",
  "green" = "#B0E794",
  "stone" = "#B4A89A",
  "dark" = "#4d4c44",
  "stone50" = "#dedbd9",
  "dark50" = "#a6a5a1",
  "stone25" = "#f2f1f0",
  "dark25" = "#dbdbda"
)

#' Function to extract F2F colours as hex codes
#'
#' @param ... Character names of F2F_colours
#'
F2F_cols <- function(...) {
  cols <- c(...)

  if (is.null(cols))
    return(F2F_colours)

  F2F_colours[cols]
}

F2F_palettes <- list(
  `main`  = F2F_cols("turquise", "orange", "blue", "rosa", "green"),
  `colblind` = c("#440154FF", "#46337EFF", "#365C8DFF", "#277F8EFF", "#1FA187FF", "#4AC16DFF", "#9FDA3AFF", "#FDE725FF"),
  `grey`  = F2F_cols("stone", "dark", "stone50", "dark50", "stone25", "dark25")#,
  # `dark` = LU_cols("blue", "bronze", "dark"),
  # `all` = LU_cols("blue", "bronze", "copper", "sky", "flower", "plaster", "stone", "dark", "stone50", "dark50", "stone25", "dark25")
)


#' Return function to interpolate a Farm2Forest color palette
#'
#' @param palette Character name of palette in F2F_palettes ("main", "colblind",
#'   "grey").
#' @param reverse Boolean indicating whether the palette should be reversed.
#' @param ... Additional arguments to pass to colorRampPalette().
#'
F2F_pals <- function(palette = "main", reverse = FALSE, ...) {
  pal <- F2F_palettes[[palette]]

  if (reverse) pal <- rev(pal)

  grDevices::colorRampPalette(pal, ...)
}


#' Colour scale constructor for F2F colours
#'
#' @param palette Character name of palette in F2F_palettes ("main").
#' @param discrete Boolean indicating whether colour aesthetic is discrete or
#'   not.
#' @param reverse Boolean indicating whether the palette should be reversed.
#' @param ... Additional arguments passed to discrete_scale() or
#'   scale_color_gradientn(), used respectively when discrete is TRUE or FALSE
#' @details The palette is made up of the different colours for the Farm2Forest project. The choices are:
#' \itemize{
#'    \item main - the main colours.
#'    \item colblind - gives a colour-blind friendly colour palette.
#'    \item grey - Six shades of grey colours, the same used by Lund university
#'  }
#' @export
scale_colour_F2F <- function(palette = "main", discrete = TRUE, reverse = FALSE, ...) {
  pal <- F2F_pals(palette = palette, reverse = reverse)

  if (discrete) {
    discrete_scale("colour", paste0("F2F_", palette), palette = pal, ...)
  } else {
    scale_color_gradientn(colours = pal(256), ...)
  }
}

#' Fill scale constructor for Lund university colours
#'
#' @param palette Character name of palette in LU_palettes ("main", "colblind",
#'   "grey").
#' @param discrete Boolean indicating whether colour aesthetic is discrete or
#'   not.
#' @param reverse Boolean indicating whether the palette should be reversed.
#' @param ... Additional arguments passed to discrete_scale() or
#'   scale_fill_gradientn(), used respectively when discrete is TRUE or FALSE.
#' @details The palette is made up of the different colours for the Farm2Forest project. The choices are:
#' \itemize{
#'    \item main - the main colours.
#'    \item colblind - gives a colour-blind friendly colour palette.
#'    \item grey - Six shades of grey colours, the same used by Lund university
#'    }
#' @export
#'
scale_fill_F2F <- function(palette = "main", discrete = TRUE, reverse = FALSE, ...) {
  pal <- F2F_pals(palette = palette, reverse = reverse)

  if (discrete) {
    discrete_scale("fill", paste0("F2F_", palette), palette = pal, ...)
  } else {
    scale_fill_gradientn(colours = pal(256), ...)
  }
}